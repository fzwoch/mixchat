package main

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/websocket"
)

type channelID struct {
	ID int `json:"id"`
}

type endpoints struct {
	Endpoints []string `json:"endpoints"`
}

type messageDataText struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type messageDataObject struct {
	Message []messageDataText `json:"message"`
}

type messageData struct {
	Message  messageDataObject `json:"message"`
	UserName string            `json:"user_name"`
}

type message struct {
	Type      string      `json:"type"`
	Method    string      `json:"method"`
	Event     string      `json:"event"`
	Error     interface{} `json:"error"`
	Data      messageData `json:"data"`
	Arguments []int       `json:"arguments"`
	ID        int         `json:"id"`
}

const (
	resetCode = "\x1b[0m"
	grayCode  = "\x1b[38;2;168;168;168m"
)

func getColorCode(s string) string {
	h := fnv.New32a()
	h.Write([]byte(s))

	return "\x1B[38;5;" + strconv.Itoa(int(h.Sum32()%256)) + "m"
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage:", os.Args[0], "<channel>")
		return
	}

	log.SetFlags(log.Ltime)
	log.SetPrefix(grayCode)

	resp, err := http.Get("https://mixer.com/api/v1/channels/" + os.Args[1])
	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	resp.Body.Close()

	id := channelID{}

	err = json.Unmarshal(body, &id)
	if err != nil {
		panic(err)
	}

	resp, err = http.Get("https://mixer.com/api/v1/chats/" + strconv.Itoa(id.ID))
	if err != nil {
		panic(err)
	}

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	resp.Body.Close()

	endpoints := endpoints{}

	err = json.Unmarshal(body, &endpoints)
	if err != nil {
		panic(err)
	}

	ws := websocket.Dialer{}

	var conn *websocket.Conn

	for _, s := range endpoints.Endpoints {
		conn, _, err = ws.Dial(s, nil)
		if err != nil {
			continue
		}
		break
	}

	if conn == nil {
		panic("could not connect")
	}
	defer conn.Close()

	msg := message{
		Type:      "method",
		Method:    "auth",
		Arguments: []int{id.ID},
		ID:        0,
	}

	s, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}

	err = conn.WriteMessage(websocket.TextMessage, s)
	if err != nil {
		panic(err)
	}

	for {
		msgType, p, err := conn.ReadMessage()
		if err != nil {
			panic(err)
		}

		if msgType != websocket.TextMessage {
			continue
		}

		err = json.Unmarshal(p, &msg)
		if err != nil {
			panic(err)
		}

		switch msg.Type {
		case "reply":
			if msg.Error != nil || msg.ID != 0 {
				panic("login failed")
			}
			log.Println(resetCode + "- Started -")
		case "event":
			switch msg.Event {
			case "ChatMessage":
				msgLine := getColorCode(msg.Data.UserName) + msg.Data.UserName + ": " + resetCode

				for _, text := range msg.Data.Message.Message {
					msgLine += strings.TrimSpace(text.Text)
				}

				log.Println(msgLine)
			}
		}
	}
}
