# MixChat

A simple console reader for mixer.com chats.

## Usage

```shell
mixchat <channel>
```

## Screenshot

![Screenshot](mixchat.png "Mixchat screenshot")
